import { Component } from '@angular/core';
import {  NavController } from 'ionic-angular';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, private faio: FingerprintAIO) {
  }

  login() {
    this.faio.show({
      clientId: 'Fingerprint-Demo',
      clientSecret: 'password', // Only Android
      localizedFallbackTitle: 'Use Pin', // Only iOS
      localizedReason: 'Please authenticate' // Only iOS
    })
      .then((result: any) => {
        console.log('OK with: ' + result);
        this.navCtrl.setRoot(HomePage);
      })
      .catch((error: any) => {
        console.log('err: ', error);
      });
  }
}

